# HomeTask_Lesson3

Вы ознакомились с основами работы с системой контроля версий Git, и теперь вам нужно создать ваш первый репозиторий с набором правок. Вам необходимо:



Пройти игру по ссылке https://learngitbranching.js.org/
Прочитать документ https://docs.unrealengine.com/en-US/Engine/UI/SourceControl/index.html#statusicons
Создать публичный репозиторий на gitlab.com
Создать проект Unreal Engine 4
Проинициализировать локальный репозиторий из Unreal Engine 4
Добавить удаленный репозиторий на gitlab к вашему локальному репозиторию
Создать 4 коммита в репозитории Unreal Engine 4 в ветке master
Создать ветку homework_3 от второго коммита в master
Создать 2 коммита в репозитории Unreal Engine 4 в ветке homework_3
Слить ветку homework_3 в master с помощью команды git merge
Отправить результаты работы в удаленный репозиторий с помощью команды git push